#pragma once

extern const char* const LIONCOIN_VERSION_TAG;
extern const char* const LIONCOIN_VERSION;
extern const char* const LIONCOIN_RELEASE_NAME;
extern const char* const LIONCOIN_VERSION_FULL;
